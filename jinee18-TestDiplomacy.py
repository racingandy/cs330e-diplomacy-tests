#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]")


    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\nC Austin\nD London")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_solve_5(self):
        r = StringIO("A Austin Move Houston\nB Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin")

    def test_solve_6(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\nC Austin\nD London")

    def test_solve7(self):
        r = StringIO("A Madrid Support B\nB Austin Hold\nC Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]")

    def test_solve8(self):
        r = StringIO("A Madrid Move Qatar\nB Austin Hold\nC Houston Move Austin\nD Jersey Support E\nE Qatar Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Jersey\nE Qatar")

    def test_solve9(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    def test_solve10(self):
        r = StringIO("A Austin Move Houston\nB Houston Move Austin\nC Dallas Support A\nD Seoul Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin\nC Dallas\nD Seoul")

# ----
# main
# ----


if __name__ == "__main__": # pragma: no cover
    main()
